package com.leroy.endereco.controller;

import com.leroy.domain.Endereco;
import com.leroy.endereco.exception.PostalServiceUnavailableException;
import com.leroy.endereco.response.ErrorResponse;
import com.leroy.endereco.response.MessageResponse;
import com.leroy.endereco.service.EnderecoService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.PathParam;

import static com.leroy.endereco.response.Messages.INVALID_ZIP_CODE_FORMAT;
import static com.leroy.endereco.response.Messages.ZIP_CODE_NOT_FOUND;

/**
 * Classe que expõe o serviço de endereço
 */
@RestController
@RequestMapping("/endereco")
public class EnderecoController {

    /*
    Injeção do serviço de enredeço
     */
    @Autowired
    private EnderecoService _enderecoService;

    /**
     * Método que inicia a API da aplicação para a busca de endereço
     * @param cep
     * @return ResponseEntity
     */
    @ApiOperation(value = "Método que executa a busca de dados de um endereço com base no CP", response = Endereco.class, produces = "application/json")
    @ApiImplicitParams({@ApiImplicitParam(name = "token", dataType = "String", required = true, paramType = "header")})
    @GetMapping(path = "/consulta-cep/{cep}")
    public ResponseEntity<?> consultaCEP(@PathVariable("cep") String cep) {
        try {

            validateCEPNumber(cep);

            Endereco endereco = _enderecoService.consultaCEP(cep);

            if(endereco == null) {
                return new ResponseEntity<MessageResponse>(new MessageResponse(ZIP_CODE_NOT_FOUND), HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<Endereco>(endereco, HttpStatus.OK);

        } catch(IllegalArgumentException e) {
            return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        } catch(PostalServiceUnavailableException e) {
            return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_GATEWAY);
        }
    }

    private void validateCEPNumber(String cep) {
        Assert.isTrue(cep.length() == 8, INVALID_ZIP_CODE_FORMAT);
    }
}
