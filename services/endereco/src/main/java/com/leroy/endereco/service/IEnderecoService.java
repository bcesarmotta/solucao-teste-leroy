package com.leroy.endereco.service;

import com.leroy.domain.Endereco;
import com.leroy.endereco.exception.PostalServiceUnavailableException;

/**
 * Interface de métodos utilizados para manipulação de endereço
 */
public interface IEnderecoService {

    Endereco consultaCEP(String cep) throws PostalServiceUnavailableException;
}
