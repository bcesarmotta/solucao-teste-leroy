package com.leroy.endereco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * start para a descoberta do serviço de endereço
 * Scneamento do pacote .com.leroy.domain
 * Start da aplicação de endereço
 */
@EnableDiscoveryClient
@EntityScan("com.radarveicular.domain")
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
