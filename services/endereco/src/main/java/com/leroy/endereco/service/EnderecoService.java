package com.leroy.endereco.service;

import com.leroy.domain.Endereco;
import com.leroy.endereco.exception.PostalServiceUnavailableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.leroy.endereco.response.Messages.UNAVAILABLE_POSTAL_SERVICE_MESSAGE;

/**
 * Classe que implementa a interface de métodos utilizados para manipulação de endereço
 */
@Service
public class EnderecoService implements IEnderecoService {

    private String VIACEP_URL = "https://viacep.com.br/ws/{cep}/";
    private String RETURN_TYPE = "json";

    @Autowired
    private RestTemplate template;

    /**
     * Método que busca dados de um endereço com base no CEP
     * @param cep
     * @return Endereco
     * @throws PostalServiceUnavailableException
     */
    @Override
    public Endereco consultaCEP(String cep) throws PostalServiceUnavailableException {

        try {

            Endereco endereco = template.getForObject( this.VIACEP_URL + this.RETURN_TYPE, Endereco.class, cep);
            return endereco;

        } catch(Exception e) {
            throw new PostalServiceUnavailableException(UNAVAILABLE_POSTAL_SERVICE_MESSAGE);
        }
    }
}
