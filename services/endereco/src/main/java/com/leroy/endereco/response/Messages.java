package com.leroy.endereco.response;

/**
 * Mensagens que devem ser retornadas ao usuário durante algum momento da aplicação
 */
public final class Messages {
    public static final String UNAVAILABLE_POSTAL_SERVICE_MESSAGE = "O serviço de consulta de CEP está temporariamente indisponível";
    public static final String ZIP_CODE_NOT_FOUND = "Não foram encontrados resultados para o cep informado";
    public static final String INVALID_ZIP_CODE_FORMAT = "O formato do cep informado não é válido";
    public static final String TOKEN_REQUIRED = "O token deve ser informado no header da requisição";
    public static final String INVALID_TOKEN = "O token informado não é válido";
}
