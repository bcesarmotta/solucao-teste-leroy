package com.leroy.endereco.exception;

/**
 * Exception criada para quando o serviço de busca de endereço noã estiver disponível
 */
public class PostalServiceUnavailableException extends Exception{
        public PostalServiceUnavailableException(String message) {
            super(message);
        }
}