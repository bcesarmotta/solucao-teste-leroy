package com.leroy.endereco.interceptor;

import com.leroy.domain.Token;
import com.leroy.domain.Usuario;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.security.Key;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import static com.leroy.endereco.response.Messages.INVALID_TOKEN;
import static com.leroy.endereco.response.Messages.TOKEN_REQUIRED;

/**
 * Interceptor criado para validar a utilização do token na API de endereços
 */
@Component
public class AuthenticationInterceptor implements HandlerInterceptor {

    private String TOKEN_URL;

    public static final String TOKEN = "token";

    @Autowired
    private DiscoveryClient _discoveryClient;

    @Autowired
    private RestTemplate template;

    /**
     * Método pré-consulta que valida o acesso do usuário na api
     * @param request
     * @param response
     * @param handler
     * @return boolean
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if(!tokenExists(request)) {
            response.sendError(401, TOKEN_REQUIRED);
            return false;
        }

        String token = request.getHeader(TOKEN);

        if(!isValidToken(token)) {
            response.sendError(401, INVALID_TOKEN);
            return false;
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

    /**
     * Método que valida se o token existe no header d requisição
     * @param request
     * @return
     */
    private boolean tokenExists(HttpServletRequest request) {

        if(request.getHeader(TOKEN) != null && !request.getHeader(TOKEN).isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Método que verifica se o token ainda é válido na requisição
     * @param token
     * @return boolean
     */
    public boolean isValidToken(String token) throws ParseException {

        System.out.println(token);

        _discoveryClient.getInstances("token").forEach(

                (ServiceInstance s) -> {
                    this.TOKEN_URL = "http://" + s.getHost() + ":" + s.getMetadata().get("management.port") + "/token/get-details?" + TOKEN + '=' + token;
                }
        );

        Token tokenDetails = template.getForObject(this.TOKEN_URL, Token.class);

        Date dataExpiracao = tokenDetails.getDataExpiracao();
        Date dataAtual = new Date();

        if(dataExpiracao.compareTo(dataAtual) < 0) {
            return false;
        }

        return true;
    }
}
