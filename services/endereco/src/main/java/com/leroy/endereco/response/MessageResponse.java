package com.leroy.endereco.response;

/**
 * Padrão de mensagens que devem ser retornadas ao usuário caso ocorra alguma exception durante uma requisição
 */
public class MessageResponse {
    private String message;

    public MessageResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
