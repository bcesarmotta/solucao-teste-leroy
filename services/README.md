
# Solução de micro serviços para busca de CEP e authenticação de usuário

Este projeto é um teste desenvolvido para a empresa Leroy Merlin, que tem o objetivo de comprovar os conhecimentos na utilização das tecnologias propostas na especificação do teste.
A camada de serviços da aplicação foi separada em módulos utilizando conceitos de decomposição de software proposto pelas arquiteturas de micro serviços.


# Módulos da aplicação

 - domain
   Módulo responsável por manter as classes de domínio do contexto da aplicação, seguindo os conceitos de DRY (Dont repear yourself) evitando assim a repetição desnecessária destes objetos.
   
 - registryServer
   Módulo responsável por registrar e monitorar os serviços "UP" da aplicação utilizando as bibliotecas da Spring Cloud.

 - token
   Módulo responsável por manipular as operações relacionadas ao token que o usuário utilizará para trafegar entre os serviços
   
 - authentication
   Módulo responsável por manipular as requisições de autenticação, validação de usuário e geração de token
   
 - endereco
   Módulo responsável por manipular e controlar o acesso aos serviços de endereço
   

# Tecnologias

- Java 8
- Spring Boot 1.5.9.RELEASE
    - spring-boot-starter
    - spring-boot-starter-data-mongodb
    - spring-boot-starter-web
    - spring-boot-starter-test    
- Spring Cloud Edgware.RELEASE
    - spring-cloud-starter-eureka-server
    - spring-cloud-starter-eureka
- Tomcat (Embedded no Spring Boot)
- MongoDB 3.6.4
- Json Web Token 0.7.0


# Execução

1. Iniciar o Mongo DB
  - ``` sudo service mongod start```
  - ``` mongo```
   
2. Criar um novo database chamado 'teste-leroy' no mongo
  -  ``` use teste-leroy```

3. Criar uma Collection chamada usuarios
   - ``` db.createCollection('usuarios')```

4. Inserir na collection de usuários um novo usuário que deverá ser utilizado no teste 
   - ``` db.collection.insert(, {name: 'bcesarmotta', password: 123})```

5. Abrir um novo terminal, acessar o diretório root dos serviços e instalar as dependências dos módulos do projeto
   - ``` cd /var/www/html/leroy/services```
   - ``` mvn clean install```

6. Abrir um novo terminal, acessar o diretório do módulo Registry Server e executar o start da aplicação 
   - ``` cd /var/www/html/leroy/services/registryService```
   - ``` mvn spring-boot:run```
   - O serviço estará disponível em http://localhost:8082

7. Abrir um novo terminal, acessar o diretório do módulo token e executar o start da aplicação
   - ``` cd /var/www/html/leroy/services/token```
   - ``` mvn spring-boot:run```
   - ``` O serviço estará disponível em http://localhost:8084```
   - A documentação do serviço estará disponível em localhost:8084/swagger-ui.html

8. Abrir um novo terminal, acessar o diretório do módulo authentication e executar o start da aplicação
   - ``` cd /var/www/html/leroy/services/authentication```
   - ``` mvn spring-boot:run```
   - ``` O serviço estará disponível em http://localhost:8083```
   - A documentação do serviço estará disponível em localhost:8083/swagger-ui.html

9. Abrir um novo terminal, acessar o diretório do módulo endereco e executar o start da aplicação
   - ``` cd /var/www/html/leroy/services/endereco```
   - ```mvn spring-boot:run```
   - ``` O serviço estará disponível em http://localhost:8088```
   - A documentação do serviço estará disponível em localhost:8088/swagger-ui.html
   
   
Collections utilizadas para a realização de testes: 
https://www.getpostman.com/collections/e5903babdf02d583146d
