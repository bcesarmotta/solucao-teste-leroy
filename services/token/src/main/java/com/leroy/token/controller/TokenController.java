package com.leroy.token.controller;

import com.leroy.domain.Endereco;
import com.leroy.domain.Token;
import com.leroy.domain.Usuario;
import com.leroy.token.exception.TokenCreationException;
import com.leroy.token.response.ErrorResponse;
import com.leroy.token.service.ITokenService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Classe que expõe o serviço de token
 */
@RestController
@RequestMapping("/token")
public class TokenController {

    /**
     * Instância o serviço da aplicação
     */
    @Autowired
    private ITokenService _tokenService;

    /**
     * Método que inicia a API da aplicação para a geração de um novo token
     * @param usuario
     * @return ResponseEntity
     */
    @ApiOperation(value = "Método que executa a criação de um novo token", response = Token.class, produces = "application/json")
    @PostMapping(value = "/new")
    public ResponseEntity<?> generateNewToken(@RequestBody Usuario usuario) {

        try {

            Token token = _tokenService.createNewToken(usuario);
            return new ResponseEntity<Token>(token, HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }

    }

    @ApiOperation(value = "Método que executa a criação de um novo token", response = Token.class, produces = "application/json")
    @GetMapping(value = "get-details")
    public ResponseEntity<?> getTokensDetails(@RequestParam("token") String token) {

        Token tokenDetails = _tokenService.getTokensDetails(token);
        return new ResponseEntity<Token>(tokenDetails, HttpStatus.OK);
    }
}

