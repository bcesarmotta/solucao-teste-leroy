package com.leroy.token.service;

import com.leroy.domain.Token;
import com.leroy.domain.Usuario;
import com.leroy.token.exception.TokenCreationException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.springframework.stereotype.Service;
import java.security.Key;
import java.util.Base64;
import java.util.Date;

import static com.leroy.token.response.Messages.TOKEN_CREATION_ERROR;

/**
 * Classe que implementa a interface de métodos utilizados para manipulação de token
 */
@Service
public class TokenService implements ITokenService {

    private static final Key secret = MacProvider.generateKey(io.jsonwebtoken.SignatureAlgorithm.HS256);
    private static final byte[] secretBytes = secret.getEncoded();
    private static final String base64SecretBytes = "6eFhStRoCLGVcRMdXd+O69Fu+qrFaQxrEzor3ddVXGU=";

    /**
     * Método para criação de um novo token para o usuário informado
     * @param usuario
     * @return Token
     * @throws Exception
     */
    public Token createNewToken(Usuario usuario) throws TokenCreationException {

        try {

            Date dataExpiracao = new Date(new Date().getTime() + (5000 * 60 * 60 * 24));

            return new Token(
                    Jwts.builder()
                            .claim("username", usuario.getName())
                            .signWith(io.jsonwebtoken.SignatureAlgorithm.HS256, base64SecretBytes)
                            .setExpiration(dataExpiracao)
                            .compact(), dataExpiracao
            );

        } catch(Exception e) {
            throw new TokenCreationException(TOKEN_CREATION_ERROR);
        }
    }

    @Override
    public Token getTokensDetails(String token) {


        Claims claims = Jwts.parser()
                .setSigningKey(base64SecretBytes)
                .parseClaimsJws(token).getBody();

        return new Token(
                token,
                claims.get("exp", Date.class));
    }


}
