package com.leroy.token.service;


import com.leroy.domain.Token;
import com.leroy.domain.Usuario;
import com.leroy.token.exception.TokenCreationException;

/**
 * Interface de métodos utilizados para manipulação de token
 */
public interface ITokenService {

    /**
     * Método para criação de um novo token para o usuário informado
     * @param usuario
     * @return Token
     * @throws Exception
     */
    public Token createNewToken(Usuario usuario) throws Exception;

    /**
     * Método que retorna dados de um token criado
     * @param token
     * @return Token
     */
    public Token getTokensDetails(String token);

}
