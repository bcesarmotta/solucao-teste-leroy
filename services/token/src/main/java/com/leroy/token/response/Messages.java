package com.leroy.token.response;

/**
 * Mensagens que devem ser retornadas ao usuário durante algum momento da aplicação
 */
public final class Messages {
    public static final String TOKEN_GENERIC_ERROR = "Erro ao executar o serviço";
    public static final String TOKEN_CREATION_ERROR = "Erro ao criar um token para o usuário informado";
}
