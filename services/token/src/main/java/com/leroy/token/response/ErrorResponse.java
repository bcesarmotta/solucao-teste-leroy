package com.leroy.token.response;

/**
 * Padrão de mensagens de erro que devem ser retornadas ao usuário caso ocorra alguma exception durante uma requisição
 */
public class ErrorResponse {

    private String error;

    public ErrorResponse(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
