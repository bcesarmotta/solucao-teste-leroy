package com.leroy.token;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * start para a descoberta do serviço de token
 * Scneamento do pacote .com.leroy.domain
 * Start da aplicação de token
 */
@EnableDiscoveryClient
@EntityScan("com.leroy.domain")
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
