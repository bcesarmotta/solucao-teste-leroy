package com.leroy.token.exception;

/**
 * Exceção que deverá ser retornada caso o token do usuário não possa ser criado
 */
public class TokenCreationException extends NullPointerException {
    public TokenCreationException(String message) {
        super(message);
    }
}
