package com.leroy.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Classe de domínio utilizada para manipular as operações de usuario
 */
@Document(collection = "usuarios")
public class Usuario {

    @Id
    private String _id;

    @Indexed(unique = true)
    private String name;

    private String password;

    private Date dataCadastro;

    public Usuario() {

    }

    public Usuario(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "_id=" + _id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", dataCadastro=" + dataCadastro +
                '}';
    }
}
