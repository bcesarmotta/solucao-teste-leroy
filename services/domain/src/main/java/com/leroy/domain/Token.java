package com.leroy.domain;

import java.util.Date;

/**
 * Classe de domínio utilizada para manipular as operações de token
 */
public class Token {

    private String token;
    private Date dataExpiracao;

    public Token() {

    }

    public Token(String token, Date dataExpiracao) {
        this.token = token;
        this.dataExpiracao = dataExpiracao;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getDataExpiracao() {
        return dataExpiracao;
    }

    public void setDataExpiracao(Date dataExpiracao) {
        this.dataExpiracao = dataExpiracao;
    }
}

