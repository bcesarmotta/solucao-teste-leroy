package com.leroy.authentication.controller;

import com.leroy.authentication.exception.TokenServiceUnavailable;
import com.leroy.authentication.exception.UserNotFoundException;
import com.leroy.authentication.response.ErrorResponse;
import com.leroy.authentication.response.MessageResponse;
import com.leroy.authentication.service.IAuthenticationService;
import com.leroy.domain.Token;
import com.leroy.domain.Usuario;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import static com.leroy.authentication.response.Messages.PASSWORD_REQUIRED;
import static com.leroy.authentication.response.Messages.USERNAME_REQUIRED;

/**
 * Classe que expõe o serviço de autenticação
 */
@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

    @Autowired
    private IAuthenticationService _authenticationService;

    /**
     * Método que inicia a API de autenticação do usuário
     * @param usuario
     * @return ResponseEntity
     */
    @ApiOperation(value = "Método que autentica um usuário no sistema", response = Token.class, produces = "application/json")
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<?> doUserAuthentication(@RequestBody Usuario usuario) {
        try {

            validateUserInputs(usuario.getName(), usuario.getPassword());

            Token token = null;
            
            if (_authenticationService.isValidUser(usuario)) {
                token = _authenticationService.getUserToken(usuario);
            }

            return new ResponseEntity<Token>(token, HttpStatus.OK);

        } catch(UserNotFoundException e) {

            return new ResponseEntity<MessageResponse>(new MessageResponse(e.getMessage()), HttpStatus.NOT_FOUND);

        } catch(TokenServiceUnavailable e) {

            return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getMessage()), HttpStatus.SERVICE_UNAVAILABLE);

        } catch(Exception e) {

            return new ResponseEntity<ErrorResponse>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    private void validateUserInputs(String name, String password) {
        Assert.hasLength(name, USERNAME_REQUIRED);
        Assert.hasLength(password, PASSWORD_REQUIRED);
    }
}
