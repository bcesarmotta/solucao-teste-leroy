package com.leroy.authentication.exception;

/**
 * Exception criado para quando o usuário não for encontrado
 */
public class UserNotFoundException extends Exception {

    public UserNotFoundException(String message) {
        super(message);
    }

}
