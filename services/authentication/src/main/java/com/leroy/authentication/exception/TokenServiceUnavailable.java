package com.leroy.authentication.exception;

import java.net.ConnectException;

/**
 * Exception criada para quando o serviço de token estiver inativo
 */
public class TokenServiceUnavailable extends ConnectException {
    public TokenServiceUnavailable(String message) {
        super(message);
    }
}
