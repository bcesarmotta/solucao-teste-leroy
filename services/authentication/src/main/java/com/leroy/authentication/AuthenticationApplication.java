package com.leroy.authentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * start para a descoberta do serviço de autenticação
 * Scneamento do pacote .com.leroy.domain
 * Start da aplicação de autenticação
 */
@EnableDiscoveryClient
@EntityScan("com.radarveicular.domain")
@SpringBootApplication
public class AuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationApplication.class, args);
	}
}
