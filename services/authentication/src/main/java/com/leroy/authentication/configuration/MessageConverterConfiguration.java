package com.leroy.authentication.configuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.text.SimpleDateFormat;

/**
 * Configuração referente as conversões de mensagem do serviço, manupilando datas, campos nulos etc
 */
@Configuration
public class MessageConverterConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
        /*
         * Instancia o objeto MappingJackson2HttpMessageConverter e ObjectMapper para setar as
         * configurações
         */
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
        ObjectMapper objectMapper = new ObjectMapper();

        /*
         * Configura para que as datas sejam transitadas com o formado dd-MM-yyyy
         * e configura tambémm para que valores nulos não sejam informados na resposta para o client
         */
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS"))
                .setSerializationInclusion(JsonInclude.Include.NON_NULL);

        jsonConverter.setObjectMapper(objectMapper);
        return jsonConverter;
    }
}
