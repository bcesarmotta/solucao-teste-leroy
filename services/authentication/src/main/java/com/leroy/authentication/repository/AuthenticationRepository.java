package com.leroy.authentication.repository;

import com.leroy.domain.Usuario;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Classe criada para manipular as operações de autenticação
 */
public interface AuthenticationRepository extends MongoRepository<Usuario, String> {

    /**
     * Método criado para buscar um usuário no repositório de dados
     * @param name
     * @param password
     * @return Usuario
     */
    public Usuario findByNameAndPassword(String name, String password);
}
