package com.leroy.authentication.service;

import com.leroy.authentication.exception.TokenServiceUnavailable;
import com.leroy.authentication.exception.UserNotFoundException;
import com.leroy.domain.Token;
import com.leroy.domain.Usuario;

/**
 * Interface de métodos utilizados para manipulação de autenticação
 */
public interface IAuthenticationService {

    public boolean isValidUser(Usuario usuario)  throws UserNotFoundException;

    public Token getUserToken(Usuario usuario) throws TokenServiceUnavailable;
}
