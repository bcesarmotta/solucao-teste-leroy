package com.leroy.authentication.service;

import com.leroy.authentication.exception.TokenServiceUnavailable;
import com.leroy.authentication.exception.UserNotFoundException;
import com.leroy.authentication.repository.AuthenticationRepository;
import com.leroy.domain.Token;
import com.leroy.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


import static com.leroy.authentication.response.Messages.TOKEN_SERVICE_UNAVAILABLE;
import static com.leroy.authentication.response.Messages.USER_NOT_FOUND;

/**
 * Classe que implementa a interface de métodos utilizados para manipulação de autenticação
 */
@Service
public class AuthenticationService implements IAuthenticationService {

	private String TOKEN_URL;

	@Autowired
    private DiscoveryClient _discoveryClient;
	
    @Autowired
    private AuthenticationRepository _authenticationRepository;

    @Autowired
    private RestTemplate template;

    /**
     * Método que verifica se o usuário é valido na base de dados
     * @param usuario
     * @return boolean
     * @throws UserNotFoundException
     */
    @Override
    public boolean isValidUser(Usuario usuario) throws UserNotFoundException {

    	Usuario user = _authenticationRepository.findByNameAndPassword(usuario.getName(), usuario.getPassword());
    	
        if(user != null) {
            return true;
        }

        throw new UserNotFoundException(USER_NOT_FOUND);
    }

    /**
     * Método que gera um novo token para o usuário informado
     * @param usuario
     * @return Token
     * @throws TokenServiceUnavailable
     */
    @Override
    public Token getUserToken(Usuario usuario) throws TokenServiceUnavailable {
    	
        try {
            _discoveryClient.getInstances("token").forEach(

                    (ServiceInstance s) -> {
                    	this.TOKEN_URL = "http://" + s.getHost() + ":" + s.getMetadata().get("management.port") + "/token/new";                      
                    }
            );

            MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
            headers.add("Content-Type", "application/json");

            HttpEntity<Usuario> request = new HttpEntity<Usuario>(usuario, headers);

            Token token = template.postForObject(this.TOKEN_URL, request, Token.class);

            return token;

        } catch(IllegalArgumentException e) {
            throw new TokenServiceUnavailable(TOKEN_SERVICE_UNAVAILABLE);
        }
    }
}
