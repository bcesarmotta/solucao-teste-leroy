package com.leroy.authentication.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Configurações referentes a documentação da aplicação utiizando o swagger
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    /*
     * Bean que seta as configurações basicas da documentação, como os contextos que ela deve resolver para a documentação e etc
     */
    @Bean
    public Docket apiDoc() {

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.leroy.authentication"))
                .paths(regex("/.*"))
                .build()
                .apiInfo(metaData());
    }

    /*
     * Método que seta as informações básicas da documentação
     */
    private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title("Documentação do serviço de autenticação")
                .description("Esta documentação tem como finalidade descrever todos os métodos contidos no Web Service de autenticação")
                .version("1.0")
                .contact(new Contact("Bruno Cesar", "", "bruno.motta@summit-bra.com"))
                .license("Apache License Version 2.0")
                .licenseUrl("https://teste/license.com")
                .build();
    }

}
