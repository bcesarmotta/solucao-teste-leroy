package com.leroy.authentication.response;

/**
 * Mensagens que devem ser retornadas ao usuário durante algum momento da aplicação
 */
public final class Messages {
    public static final String USER_NOT_FOUND = "O usuário informado não é válido ou não existe";
    public static final String TOKEN_SERVICE_UNAVAILABLE = "O serviço de Token está temporáriamente indisponível";
    public static final String USERNAME_REQUIRED = "O nome de usuário deve ser informado";
    public static final String PASSWORD_REQUIRED = "A senha deve ser informada";
}