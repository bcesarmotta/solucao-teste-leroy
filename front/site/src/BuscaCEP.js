
import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import  { Redirect } from "react-router-dom"
import Cookies from "universal-cookie";

class Stuff extends Component {

  constructor(props) {

    super(props);
    this.state = {
      cep: "",
      rua: "",
      bairro: "",
      cidade: "",
      url_api: "",
      shouldHide: true
    };
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });

    this.state.shouldHide = true;
  }

  validateForm() {
    return this.state.cep.length > 0;
  }

  handleSubmit = event => {
    event.preventDefault();
  }

  callAdressSearch() {

    const cookies = new Cookies();

    console.log( '>>>' + cookies.get("tokenAuth").token);

    fetch('http://localhost:8088/endereco/consulta-cep/' + this.state.cep, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'token': cookies.get("tokenAuth").token
      } 
    }).then(function (response) {
      return response.json();
    }).then(data => {

      console.log(data);

      this.setState({
        rua: data.logradouro,
        bairro: data.bairro,
        cidade: data.localidade + ' ' + data.uf,
        cep: data.cep,
        url_api: data.logradouro + ',' + data.localidade + ',' + data.uf,
        shouldHide : false
      });
    });;

  }

  render() {

    const cookies = new Cookies();

    if(!cookies.get("tokenAuth")) {
      return <Redirect to='/login'/>;
    }

    return (

      <div>

        <div className="body-search">

          <h1>Consulta de Endereço</h1>

          <form onSubmit={this.handleSubmit} method="post">

            <label>CEP</label>

            <FormGroup controlId="cep" bsSize="large">

              <FormControl
                autoFocus
                type="text"
                value={this.state.cep}
                onChange={this.handleChange}
                placeholder="CEP"
              />

              <Button
                block
                bsSize="large"
                disabled={!this.validateForm()}
                type="submit"
                onClick={() => this.callAdressSearch()}
              >Pesquisar</Button>
            </FormGroup>
          </form>
        </div>

        <br />

        <div className={this.state.shouldHide ? 'hide address-content' : 'address-content'}>

          <p className="rua">{this.state.rua}</p>
          <p className="bairro">{this.state.bairro}</p>
          <p className="cidade">{this.state.cidade}</p>
          <p className="cep">{this.state.cep}</p>

          <br/>

          <iframe width="900" height="450" src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyA85derWomKtXYJsw5lwBiUCLwFxnhZtCw&q=" + this.state.url_api}></iframe>

        </div>

        
      </div>
    );
  }
}

export default Stuff;