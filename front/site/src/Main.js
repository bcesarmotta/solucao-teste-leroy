import React, { Component } from "react";
import Cookies from 'universal-cookie'
import  { Redirect } from "react-router-dom"

import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Login from "./Login";
import BuscaCEP from "./BuscaCEP";

class Main extends React.Component {

  constructor(props) {      
      super(props);
  }

  render() {

    const cookies = new Cookies();

    return (
      <div>
        <HashRouter>
          <div>
            <div className="content">
              <Route exact path="/login" component={Login} />
              <Route path="/busca-cep" component={BuscaCEP} />
              <Route exact path="/" render={() => (
                cookies.get("tokenAuth") ? (
                  <Redirect to="/busca-cep"/>
                ) : (
                  <Redirect to="/login"/>
                )
              )}/>
            </div>
          </div>
        </HashRouter>
        
      </div>
    );
  }
}

export default Main;