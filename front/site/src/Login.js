import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import  { Redirect } from "react-router-dom"
import Cookies from "universal-cookie";

class Home extends React.Component {

    constructor(props) {

        const cookies = new Cookies();

        super(props);

        this.state = {
            email: "",
            password: ""
        };

    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();
    }

    callUserAuth() {
        fetch('http://localhost:8083/authentication/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: this.state.email,
                password: this.state.password,
            })
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            const cookies = new Cookies();
            cookies.set('tokenAuth', data);
            window.location.reload();
        });
    }

    render() {

        const cookies = new Cookies();

        console.log('111');

       if(cookies.get("tokenAuth")) {
            return <Redirect to='/busca-cep'/>;
       }

       console.log('222');

        return (
            <div>
                <img src="leroy-logo.png" className="logotipo" />

                <div className="Login">
                    <form onSubmit={this.handleSubmit} method="post">
                        <FormGroup controlId="email" bsSize="large">
                            <FormControl
                                autoFocus
                                type="text"
                                value={this.state.email}
                                onChange={this.handleChange}
                                placeholder="Email"
                            />
                        </FormGroup>
                        <FormGroup controlId="password" bsSize="large">
                            <FormControl
                                value={this.state.password}
                                onChange={this.handleChange}
                                type="password"
                                placeholder="Password"
                            />
                        </FormGroup>
                        <Button
                            block
                            bsSize="large"
                            disabled={!this.validateForm()}
                            type="submit"
                            onClick={() => this.callUserAuth()}
                        >
                            Login
                        </Button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Home;