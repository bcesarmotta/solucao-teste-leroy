
# Solução de micro serviços para busca de CEP e authenticação de usuário

Este projeto é um teste desenvolvido para a empresa Leroy Merlin, que tem o objetivo de comprovar os conhecimentos na utilização das tecnologias propostas na especificação do teste.
A camada de views da aplicação foi desenvolvida utilizando o framework ReactJS


# Páginas da aplicação

 - Login
   Página utilizada para autenticar o usuário nos serviços desenvolvidos.
   
 - consulta-cep
   Página utilizada paga busca e consulta de cep. Esta view só pode ser acessada caso o usuário esteja logado.

# Execução

1. Acessar a pasta root do front e executar o seguite comando
  - ``` npm start```